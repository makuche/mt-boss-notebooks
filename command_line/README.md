## 1 Baseline

BOSS command line usage is introduced in https://cest-group.gitlab.io/boss/tutorials/quickstart_cli.html

See examples baseline\_[task]\_[dimension].in

To run an example, have BOSS installed and write

````
$ boss o input_file_name.in
````

BOSS writes optimisation outcome in an output file and creates a restart file that can be used to continue a run as explained in https://cest-group.gitlab.io/boss/tutorials/restarting_cli.html

## 2 Warm start

Assume that some input-output data is available that describes a task that is related to our optimisation task. This can be a previous optimisation run or other experiments carried out in the same input domain. BOSS can use the related data to initialise the model used in optimisation, ie warm start optimisation in the new task.

Minimal modifications compared to a baseline setup:
- add `num_tasks 2`
- define initpts for both tasks: `initpts N_target N_related`
- add the support task data under RESULTS. There must be N_related lines and each line must contain information about one data point in the format: `x_1 x_2 … x_d 1 y` where x_i are the input values and y is the observed output

See examples warm\_start\_[task]\_[dimension]*

## 3 Multi-task initialisation

BOSS can also acquire initialisation data for several tasks at the same time. This can reduce overall computational cost in optimisation in case previous data is not available but a related task exists that is much cheaper to evaluate than the task we wish to optimise.

Now we provide optimisation two user functions. Minimal modifications compared to a baseline setup:
- add `num_tasks 2`
- define user_fn for both tasks as a semicolon-separated list. The function we wish to optimise comes first: `user_fn target_f.py; related_f.py`
- define initpts for both tasks: `initpts N_target N_related`

See examples multi_init_[task]_[dimension].in

## 4 Multi-task optimisation

Access to one or more related tasks also makes it possible to use multi-task optimisation. In this case optimisation decides on each iteration both the input parameters and the task to evaluate. Multi-task optimisation uses a cost-aware acquisition function that takes into account the acquisition cost associated with each task. 

The current version relies on the user to provide acquisition costs and a maximum cost as input. The maximum cost is used as an additional stopping condition in optimisation. In practice optimisation continues until `iterpts` iterations are finished or until the next evaluation would exceed the cost limit `maxcost`. Minimal modifications compared to a baseline setup:

- add `num_tasks 2`
- define user_fn for both tasks as a semicolon-separated list. The function we wish to optimise comes first: `user_fn target_f.py; related_f.py`
- add acquisition costs in the same order: `acqcost cost_target cost_related`
- add `maxcost cost_limit` and increase iterpts

See examples multi\_[task]\_[dimension]*

## Notes

BOSS uses the intrinsic coregionalisation model (ICM) to capture dependencies between tasks in use cases 2-4. The multi-task model has more hyperparameters than the model used in baseline optimisation. The new hyperparameters can be associated with prior distributions with the new input options:
- W_prior
- W_priorpar
- kappa_prior
- kappa_priorpar

See examples [case]\_[task]\_[dimension]_example_prior.in

When baseline optimisation is used, BOSS can set the prior distribution parameters based on heuristic rules. [Sten (2021)](http://urn.fi/URN:NBN:fi:aalto-202101311714) studied possible heuristics that could be used to achieve the same with the new multi-task model parameters. 

Prior distributions set based on the recommended heuristic are available as examples [case]\_[task]\_[dimension]_heuristic_22.in
