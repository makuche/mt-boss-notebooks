Here are models and utilities that can be used to test [BOSS](https://gitlab.com/cest-group/boss) in an optimisation task that mimics atomistic structure search. The contents are organised as follows:

- model: regression models fitted on (input, output) data collected in past optimisation experiments

- notebooks: example notebooks show how the models can be loaded and used as the unknown cost function in BOSS

- command_line: includes python functions that return predictions from the saved models and example input files to command-line BOSS

The examples have been tested with the model-class and multi-task extensions to BOSS 1.2. This resource is not maintained.